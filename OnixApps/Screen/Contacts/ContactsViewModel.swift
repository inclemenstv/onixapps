//
//  ContactsViewModel.swift
//  MVVM-2
//
//  Created by Konstantin Pischanskyi on 04.01.2020.
//

import Foundation

protocol ContactsTableViewModelType {
    
    func numberOfRows() -> Int
    func cellViewModel(forIndexPath indexPath: IndexPath) -> ContactsTableViewCellModelType?

}


class ContactsViewModel: ContactsTableViewModelType  {
   
    
    
    
    var profiles = [
               Profile(id: 33),
               Profile(id: 21),
               Profile(id: 55)]
    
    
    

    func numberOfRows() -> Int {
        return profiles.count
       }
    
    func cellViewModel(forIndexPath indexPath: IndexPath) -> ContactsTableViewCellModelType? {
        let profile = profiles[indexPath.row]
        
        return ContactsTableViewCellViewModel(profile: profile)
    }
  
}

