

import UIKit

class ContactsTableViewController: UITableViewController {

    var viewModel : ContactsTableViewModelType?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel = ContactsViewModel()
    }
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return viewModel?.numberOfRows() ?? 0
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? ContactsTableViewCell
        
        guard let tableViewCell = cell,
        let viewModel = viewModel else { return UITableViewCell() }
        
        let cellViewModel = viewModel.cellViewModel(forIndexPath: indexPath)
        tableViewCell.viewModel = cellViewModel
//        let profile = viewModel.profiles[indexPath.row]
//        
//        tableViewCell.id.text = "\(profile.id)"

        return tableViewCell
    }
}
