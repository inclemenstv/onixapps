//
//  TableViewCell.swift
//  MVVM-2
//
//  Created by Ivan Akulov on 12/05/2018.
//  Copyright © 2018 Ivan Akulov. All rights reserved.
//

import UIKit






class ContactsTableViewCell: UITableViewCell {


    @IBOutlet weak var id: UILabel!
    
    weak var viewModel: ContactsTableViewCellModelType? {
        willSet(viewModel) {
            
            guard let viewModel = viewModel else {return}
            id.text = "\(viewModel.id)"
        }
    }
    
}
