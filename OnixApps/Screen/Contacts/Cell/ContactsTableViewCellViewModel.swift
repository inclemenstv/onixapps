//
//  ContactsTableViewCellViewModel.swift
//  MVVM-2
//
//  Created by Konstantin Pischanskyi on 04.01.2020.
//  Copyright © 2020 Ivan Akulov. All rights reserved.
//

import Foundation



protocol ContactsTableViewCellModelType: class {
    var id: Int {get}
}



class ContactsTableViewCellViewModel: ContactsTableViewCellModelType {
    
    
    private var profile: Profile
    
    var id: Int {
        return profile.id
    }
    
    init(profile: Profile) {
        self.profile = profile
    }
    
}
